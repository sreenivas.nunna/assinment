package com.itgrids.githubassignment.model;

public class Filter {
    public String createdFrom;
    public String createdTo;
    public String customQuery;
    public String orderBy = "desc";
    public int pageNo = 1;
    public int perPage = 10;
    public String queryBy = "sandeep";
    public String sortBy = "watcher";

    public String getQueryBy() {
        return this.queryBy;
    }

    public void setQueryBy(String queryBy) {
        this.queryBy = queryBy;
    }

    public String getCustomQuery() {
        if (this.createdTo != null) {
            return this.queryBy + " created:" + this.createdFrom + ".." + this.createdTo;
        }
        return this.queryBy;
    }

    public void setCustomQuery(String customQuery) {
        this.customQuery = customQuery;
    }

    public int getPageNo() {
        return this.pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPerPage() {
        return this.perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getSortBy() {
        return this.sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getOrderBy() {
        return this.orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getCreatedFrom() {
        return this.createdFrom;
    }

    public void setCreatedFrom(String createdFrom) {
        this.createdFrom = createdFrom;
    }

    public String getCreatedTo() {
        return this.createdTo;
    }

    public void setCreatedTo(String createdTo) {
        this.createdTo = createdTo;
    }

    public void clearFilter() {
        this.sortBy = "watcher";
        this.orderBy = "desc";
        this.createdTo = null;
        this.createdFrom = null;
        this.perPage = 10;
        this.pageNo = 1;
        this.queryBy = "sandeep";
    }

    public String toString() {
        return super.toString();
    }
}
