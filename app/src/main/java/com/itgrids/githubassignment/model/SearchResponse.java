package com.itgrids.githubassignment.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class SearchResponse {
    @SerializedName("incomplete_results")
    public boolean incompleteResults;
    @SerializedName("items")
    public ArrayList<Repo> repos;
    @SerializedName("total_count")
    public int totalCount;

    public int getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public boolean isIncompleteResults() {
        return this.incompleteResults;
    }

    public void setIncompleteResults(boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public ArrayList<Repo> getRepos() {
        return this.repos;
    }

    public void setRepos(ArrayList<Repo> repos) {
        this.repos = repos;
    }
}
