package com.itgrids.githubassignment.utils;


import com.itgrids.githubassignment.model.Contributor;
import com.itgrids.githubassignment.model.Filter;
import com.itgrids.githubassignment.model.Repo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sys on 7/6/2016.
 */
public class GlobalAccess {
    private static GlobalAccess ourInstance = new GlobalAccess();
    public static GlobalAccess getInstance()
    {
        return ourInstance;
    }

    public Repo getRepo() {
        return repo;
    }

    public void setRepo(Repo repo) {
        this.repo = repo;
    }

    private Repo repo;

    public Contributor getmContributor() {
        return mContributor;
    }

    public void setmContributor(Contributor mContributor) {
        this.mContributor = mContributor;
    }

    private Contributor mContributor;

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    private Filter filter;


}
