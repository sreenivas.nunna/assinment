package com.itgrids.githubassignment.utils;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;


import java.util.List;


/**
 * Created by sys on 7/2/2016.
 */
public class MultiDexApplication extends Application
{
    private static Context mContext;

    @Override
    public void onCreate()
    {
        super.onCreate();
        try
        {
            MultiDex.install(this);
        }catch (Exception e)
        {
            e.printStackTrace();
        }


        mContext = getApplicationContext();


    }


    public static Context getMyInstance()
    {
        return mContext;
    }

}
