package com.itgrids.githubassignment.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.Window;
import android.widget.ProgressBar;

import com.itgrids.githubassignment.R;

/**
 * Created by Srinivas on 12/21/2017.
 */

public class CustamDialog {

    public static Dialog progressDialog(Context mContext)
    {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.progress_bar);
        dialog.show();

        ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.progress);
        if(Build.VERSION.SDK_INT < 21 )
        {
            progressBar.getIndeterminateDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
        }

        return dialog;
    }

}
