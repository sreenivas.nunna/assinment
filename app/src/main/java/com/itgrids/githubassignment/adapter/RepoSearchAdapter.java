package com.itgrids.githubassignment.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.itgrids.githubassignment.R;
import com.itgrids.githubassignment.model.Repo;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/*
 * Created by admin on 12/16/2017.
 */

public class RepoSearchAdapter extends RecyclerView.Adapter<RepoSearchAdapter.MyViewHolder>
{
     Context context;
     ArrayList<Repo> mRepoArrayList =new ArrayList<Repo>();
     Repo mRepoVo = new Repo();
     private ItemClickListenerIn itemClickListener;


    public RepoSearchAdapter(Context context, ArrayList<Repo> mRepoArrayList)
    {
        this.context = context;
        this.mRepoArrayList = mRepoArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        Repo mRepoVo;
        CardView card_view;
        TextView forkCount;
        TextView fullName;
        ImageView imgAvatar;
        TextView name;
        TextView starCount;
        TextView watchCount;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            imgAvatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
            name = (TextView) itemView.findViewById(R.id.name);
            fullName = (TextView) itemView.findViewById(R.id.fullName);
            watchCount = (TextView) itemView.findViewById(R.id.watchCount);
            forkCount = (TextView) itemView.findViewById(R.id.forkCount);
            starCount = (TextView) itemView.findViewById(R.id.starCount);
            card_view = (CardView) itemView.findViewById(R.id.card_view);


            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemClick(view, mRepoVo);
                }
            });*/
            card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("========","========");

                    itemClickListener.onItemClick(v, mRepoVo);
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_repo, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateandtime = simpleDateFormat.format(new Date());
        mRepoVo = mRepoArrayList.get(position);
        holder.mRepoVo = mRepoVo;

        holder.name.setText(mRepoVo.getName());
        holder.fullName.setText(mRepoVo.getFullName());
        holder.watchCount.setText(String.valueOf(mRepoVo.getWatchersCount()));
        holder.starCount.setText(String.valueOf(mRepoVo.getStargazersCount()));
        holder.forkCount.setText(String.valueOf(mRepoVo.getForksCount()));
        Picasso.with(this.context).load(mRepoVo.getOwner().getAvatarUrl()).placeholder((int)
                R.drawable.image_placeholder).error((int) R.drawable.image_placeholder).into(holder.imgAvatar);



    }

    @Override
    public int getItemCount()
    {
        return mRepoArrayList.size();
    }

    public void setClickListener(ItemClickListenerIn itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    public interface ItemClickListenerIn {
        void onItemClick(View view, Repo mRepoVo);
    }
}
