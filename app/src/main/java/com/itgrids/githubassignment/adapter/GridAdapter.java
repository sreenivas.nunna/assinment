package com.itgrids.githubassignment.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itgrids.githubassignment.R;
import com.itgrids.githubassignment.model.Contributor;
import com.itgrids.githubassignment.model.Repo;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

/*
 * Created by admin on 12/16/2017.
 */

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder>
{
     Context context;
    ArrayList<Contributor> mContributorList = new ArrayList<Contributor>();
    Contributor mContributorVo = new Contributor();
     private ItemClickListenerIn itemClickListener;


    public GridAdapter(Context context, ArrayList<Contributor> mContributorList)
    {
        this.context = context;
        this.mContributorList = mContributorList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        Contributor mContributorVo;

        CircleImageView imgAvatar;
        TextView name;



        public MyViewHolder(View itemView)
        {
            super(itemView);
            imgAvatar = (CircleImageView) itemView.findViewById(R.id.imgAvatar);
            name = (TextView) itemView.findViewById(R.id.contributorName);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemClick(view, mContributorVo);
                }
            });
            /*card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("========","========");

                    itemClickListener.onItemClick(v, mContributorVo);
                }
            });*/
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_contributor, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateandtime = simpleDateFormat.format(new Date());
        mContributorVo = mContributorList.get(position);
        holder.mContributorVo = mContributorVo;

        Picasso.with(context).load(mContributorVo.getAvatarUrl()).placeholder((int) R.drawable.git_avatar).error((int) R.drawable.git_avatar).into(holder.imgAvatar);
        holder.name.setText(mContributorVo.getLogin());

    }

    @Override
    public int getItemCount()
    {
        return mContributorList.size();
    }

    public void setClickListener(ItemClickListenerIn itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    public interface ItemClickListenerIn {
        void onItemClick(View view, Contributor mContributorVo);
    }
}
