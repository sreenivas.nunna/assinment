package com.itgrids.githubassignment.activitys;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.itgrids.githubassignment.R;
import com.itgrids.githubassignment.adapter.GridAdapter;
import com.itgrids.githubassignment.adapter.RepoSearchAdapter;
import com.itgrids.githubassignment.api.RestAPI;
import com.itgrids.githubassignment.api.RestClient;
import com.itgrids.githubassignment.model.Contributor;
import com.itgrids.githubassignment.model.Repo;
import com.itgrids.githubassignment.utils.CustamDialog;
import com.itgrids.githubassignment.utils.GlobalAccess;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Srinivas on 12/20/2017.
 */

public class RepoDetails extends AppCompatActivity implements GridAdapter.ItemClickListenerIn {

    GlobalAccess mGlobalAccess;
    Repo mRepo;

    ImageView imgAvatar;
    TextView Description;
    TextView ProjectLink;
    RecyclerView mGridView;
    RecyclerView.LayoutManager layoutManager;

    private ArrayList<Contributor> mContributors;
    private GridAdapter mGridContributorAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repo_details);

        mGlobalAccess = GlobalAccess.getInstance();
        mRepo = mGlobalAccess.getRepo();
        viewBinf();
        Picasso.with(getApplicationContext()).load(this.mRepo.getOwner().getAvatarUrl()).placeholder((int) R.drawable.avatar).error((int) R.drawable.avatar).into(imgAvatar);
        Log.e("=======","======"+mRepo.getHtmlUrl());
        ProjectLink.setText(Html.fromHtml("<u>" + "Click Here" + "</u>"));

        Description.setText(mRepo.getDescription());
        mGridView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this, 3);
        mGridView.setLayoutManager(layoutManager);
        callServiceReop();


        ProjectLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(RepoDetails.this, WebViewActivity.class);
                in.putExtra("url", mRepo.getHtmlUrl());
                startActivity(in);
            }
        });

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBack();
            }
        });
    }

    private void viewBinf() {
        imgAvatar = (ImageView) findViewById(R.id.avatar);
        Description = (TextView) findViewById(R.id.description);
        ProjectLink = (TextView) findViewById(R.id.link);
        mGridView = (RecyclerView) findViewById(R.id.list_item);

    }

    private void callServiceReop() {


        RestAPI apiService = RestClient.getClient().create(RestAPI.class);
        final Dialog mProgressDialog = CustamDialog.progressDialog(RepoDetails.this);


        Call<ArrayList<Contributor>> call = apiService.getContributors(mRepo.getOwner().getLogin(), mRepo.getName());


        call.enqueue(new Callback<ArrayList<Contributor>>() {
            @Override
            public void onResponse(Call<ArrayList<Contributor>> call, Response<ArrayList<Contributor>> response) {

                mProgressDialog.dismiss();
                if (response.code() == ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION) {
                    initRecycler(response);
                    //setGridViewHeightBasedOnChildren(contributorGrid, 3);
                    return;
                }

            }

            @Override
            public void onFailure(Call<ArrayList<Contributor>> call, Throwable t) {
                Log.e("Repo FAILED", t.getMessage().toString());
            }
        });

    }

    private void initRecycler(Response<ArrayList<Contributor>> response) {
        mContributors = (ArrayList) response.body();
        mGridContributorAdapter = new GridAdapter(RepoDetails.this, mContributors);
        mGridView.setAdapter(mGridContributorAdapter);
        mGridContributorAdapter.setClickListener(this);
        mGridContributorAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View view, Contributor mContributorVo) {

        mGlobalAccess.setmContributor(mContributorVo);
        Intent i = new Intent(RepoDetails.this, ContributeDetails.class);
        startActivity(i);
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        onBack();
    }

    public void onBack() {
        //super.onBackPressed();
        Intent i = new Intent(RepoDetails.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
