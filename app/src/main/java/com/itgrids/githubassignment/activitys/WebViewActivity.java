package com.itgrids.githubassignment.activitys;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.itgrids.githubassignment.R;


public class WebViewActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    WebView webView;

    public class WebClient extends WebViewClient {
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            WebViewActivity.this.progressDialog.show();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            WebViewActivity.this.progressDialog.show();
            view.loadUrl(url);
            return true;
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            WebViewActivity.this.progressDialog.hide();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);

        this.webView = (WebView) findViewById(R.id.webView);
        this.webView.setWebViewClient(new WebClient());
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.loadUrl(getIntent().getStringExtra("url"));
        this.webView.setHorizontalScrollBarEnabled(false);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage("Loading...");
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.webView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.webView.goBack();
        return true;
    }
}
