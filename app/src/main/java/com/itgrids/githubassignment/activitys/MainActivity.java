package com.itgrids.githubassignment.activitys;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.itgrids.githubassignment.R;
import com.itgrids.githubassignment.adapter.RepoSearchAdapter;
import com.itgrids.githubassignment.api.RestAPI;
import com.itgrids.githubassignment.api.RestClient;
import com.itgrids.githubassignment.model.Repo;
import com.itgrids.githubassignment.model.SearchResponse;
import com.itgrids.githubassignment.utils.CustamDialog;
import com.itgrids.githubassignment.utils.GlobalAccess;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements RepoSearchAdapter.ItemClickListenerIn{

    private String orderBy = "desc";
    private int pageNo = 1;
    private int perPage = 10;
    private String sortBy = "watcher";
    private String q = "Srinivas";

    RecyclerView mRecyclerView;
    private ArrayList<Repo> mRepoArrayList;
    RepoSearchAdapter mRepoSearchAdapter;
    GlobalAccess mGlobalAccess;

    SearchView searchView;
    ImageView filter;

    int position = 0;

    LinearLayout pagesView;
    LayoutInflater mLayoutInflater;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mGlobalAccess = GlobalAccess.getInstance();
        mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRecyclerView = (RecyclerView) findViewById(R.id.repo_recyclerview);
        pagesView = (LinearLayout) findViewById(R.id.pages);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        searchView = (SearchView) findViewById(R.id.searchview);
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!TextUtils.isEmpty(query) && query.length() >= 2) {
                    if (query != null) {
                        q = query;
                    }
                    callServiceReop();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });

        filter = (ImageView) findViewById(R.id.filter);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailog();
            }
        });

        callServiceReop();
    }

    private void callServiceReop() {

        final Dialog mProgressDialog = CustamDialog.progressDialog(MainActivity.this);
            RestAPI apiService = RestClient.getClient().create(RestAPI.class);


            Call<SearchResponse> call = apiService.searchRepository(q, sortBy, orderBy, perPage, pageNo);

            call.enqueue(new Callback<SearchResponse>() {
                @Override
                public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                    mProgressDialog.dismiss();
                    if (((SearchResponse) response.body()).getTotalCount() > 0) {

                        initRecycler(response);
                    }

                }

                @Override
                public void onFailure(Call<SearchResponse> call, Throwable t) {
                    Log.e("Repo FAILED", t.getMessage().toString());
                }
            });

    }
    private void initRecycler(Response<SearchResponse> response) {
        mRepoArrayList = ((SearchResponse) response.body()).getRepos();
        mRepoSearchAdapter = new RepoSearchAdapter(MainActivity.this, mRepoArrayList);
        mRecyclerView.setAdapter(mRepoSearchAdapter);
        mRepoSearchAdapter.setClickListener(this);
        mRepoSearchAdapter.notifyDataSetChanged();

        createpagesview(((SearchResponse) response.body()).getTotalCount());
    }

    private void createpagesview(final int Count) {

        int pages = Count/10;
        final TextView[] tv = new TextView[pages];
        pagesView.removeAllViews();
        for(int i=0; i<pages; i++){
            View view = mLayoutInflater.inflate(R.layout.layout_page, null);
            tv[i] = (TextView) view.findViewById(R.id.text);
            tv[i].setText(String.valueOf(i+1));
            tv[i].setTag(String.valueOf(i+1));
            if(i == pageNo-1){
                tv[i].setBackgroundColor(Color.GRAY);
            }

            tv[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pageNo = Integer.valueOf(v.getTag().toString());

                    for(int j =0; j<tv.length; j++){
                        tv[j].setBackgroundColor(Color.WHITE);
                    }
                    v.setBackgroundColor(Color.GRAY);
                    callServiceReop();
                }
            });
            pagesView.addView(view);
        }



    }

    @Override
    public void onItemClick(View view, Repo mRepoVo) {
        mGlobalAccess.setRepo(mRepoVo);
        Intent intent = new Intent(MainActivity.this, RepoDetails.class);
        startActivity(intent);
    }



    private void dailog() {

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = (int) ((int)displaymetrics.widthPixels * 0.9);
        int height = (int) ((int)displaymetrics.heightPixels * 0.9);
        dialog.getWindow().setLayout(width,height);
        dialog.show();

        RadioGroup mRadioGroup1 = (RadioGroup) dialog.findViewById(R.id.radioGroup1);
        RadioGroup mRadioGroup2 = (RadioGroup) dialog.findViewById(R.id.radioGroup2);
        RadioButton stars = (RadioButton) dialog.findViewById(R.id.stars);
        RadioButton forks = (RadioButton) dialog.findViewById(R.id.forks);
        RadioButton updated = (RadioButton) dialog.findViewById(R.id.updated);
        RadioButton desc = (RadioButton) dialog.findViewById(R.id.desc);
        RadioButton asc = (RadioButton) dialog.findViewById(R.id.asc);

        mRadioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch (checkedId)
                {
                    case R.id.stars:
                        sortBy = "stars";
                        break;
                    case R.id.forks:
                        sortBy = "forks";
                        break;
                    case R.id.updated:
                        sortBy = "updated";
                        break;
                    default:
                        break;
                }
            }
        });
        mRadioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch (checkedId)
                {
                    case R.id.desc:
                        orderBy = "desc";
                        break;
                    case R.id.asc:
                        orderBy = "asc";
                        break;
                }
            }
        });

        LinearLayout mLayout = (LinearLayout) dialog.findViewById(R.id.btn);
        mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageNo = 1;
                dialog.dismiss();
                callServiceReop();
            }
        });
    }



}
