package com.itgrids.githubassignment.activitys;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.itgrids.githubassignment.R;
import com.itgrids.githubassignment.adapter.RepoAdapter;
import com.itgrids.githubassignment.adapter.RepoSearchAdapter;
import com.itgrids.githubassignment.api.RestAPI;
import com.itgrids.githubassignment.api.RestClient;
import com.itgrids.githubassignment.model.Contributor;
import com.itgrids.githubassignment.model.Repo;
import com.itgrids.githubassignment.model.SearchResponse;
import com.itgrids.githubassignment.utils.CustamDialog;
import com.itgrids.githubassignment.utils.GlobalAccess;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Srinivas on 12/21/2017.
 */

public class ContributeDetails extends AppCompatActivity implements RepoAdapter.ItemClickListenerIn{

    ImageView mImageView;
    TextView name;
    RecyclerView mRecyclerView;
    GlobalAccess mGlobalAccess;
    private RecyclerView.LayoutManager linearLayoutManager;
    private Contributor mContributor;
    private ArrayList<Repo> mRepoArrayList;
    private RepoAdapter mRepoSearchAdapter;
    TextView mTilte;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contributer_details);
        mGlobalAccess = GlobalAccess.getInstance();
        mContributor = mGlobalAccess.getmContributor();
        viewBind();

        Picasso.with(getApplicationContext()).load(mContributor.getAvatarUrl()).
                placeholder(R.drawable.avatar).error((int) R.drawable.avatar).into(mImageView);
        name.setText(mContributor.getLogin());
        mRecyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(this.linearLayoutManager);
        callServiceReop();

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBack();
            }
        });

        mTilte = (TextView) findViewById(R.id.title);
        mTilte.setText(mContributor.getLogin());
    }

    private void viewBind() {
        mImageView = (ImageView) findViewById(R.id.imgAvatar);
        name = (TextView) findViewById(R.id.login);
        mRecyclerView = (RecyclerView) findViewById(R.id.listview);
    }

    private void callServiceReop() {

        RestAPI apiService = RestClient.getClient().create(RestAPI.class);

        final Dialog mProgressDialog = CustamDialog.progressDialog(ContributeDetails.this);
        Call<ArrayList<Repo>> call = apiService.getRepositories(mContributor.getLogin());

        call.enqueue(new Callback<ArrayList<Repo>>() {
            @Override
            public void onResponse(Call<ArrayList<Repo>> call, Response<ArrayList<Repo>> response) {
                mProgressDialog.dismiss();
                if (response.body() != null) {
                    intiRecycler(response);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Repo>> call, Throwable t) {
                Log.e("Repo FAILED", t.getMessage().toString());
            }
        });
    }

    private void intiRecycler(Response<ArrayList<Repo>> response) {
        mRepoArrayList = (ArrayList) response.body();
        mRepoSearchAdapter = new RepoAdapter(ContributeDetails.this, mRepoArrayList);
        mRecyclerView.setAdapter(mRepoSearchAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRepoSearchAdapter.setClickListener(this);
        mRepoSearchAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View view, Repo mRepoVo) {
        mGlobalAccess.setRepo(mRepoVo);
        Intent intent = new Intent(ContributeDetails.this, RepoDetails.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        onBack();
    }

    public void onBack() {
        //super.onBackPressed();
        Intent i = new Intent(ContributeDetails.this, RepoDetails.class);
        startActivity(i);
        finish();
    }
}
