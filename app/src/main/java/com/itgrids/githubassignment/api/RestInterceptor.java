package com.itgrids.githubassignment.api;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;

public class RestInterceptor implements Interceptor {
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Builder requestBuilder = original.newBuilder();
        requestBuilder.header("Authorization", "token 39c15c9a9fa2f4219836643bc49303ee1cf7f710");
        requestBuilder.header("Accept", "application/vnd.github.mercy-preview+json");
        return chain.proceed(original);
    }
}
