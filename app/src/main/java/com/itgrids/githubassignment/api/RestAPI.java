package com.itgrids.githubassignment.api;

import com.itgrids.githubassignment.model.Contributor;
import com.itgrids.githubassignment.model.Repo;
import com.itgrids.githubassignment.model.SearchResponse;

import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestAPI {
    @GET("repos/{login}/{project}/contributors")
    Call<ArrayList<Contributor>> getContributors(@Path("login") String str, @Path("project") String str2);

    @GET("users/{login}/repos")
    Call<ArrayList<Repo>> getRepositories(@Path("login") String str);

    @GET("search/repositories")
    Call<SearchResponse> searchRepository(@Query("q") String str, @Query("sort") String str2, @Query("order") String str3, @Query("per_page") int i, @Query("page") int i2);
}
